import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import axios from 'axios'
import JsonViewer from 'vue-json-viewer'

// import VCharts from 'v-charts'
import './plugins'
import './styles/global.scss'
import './utils/filters'
import noData from '@/components/NoData'
import pagination from '@/components/Pagination'
import uploadImg from '@/components/UploadImg'
import imgManager from '@/components/ImgManager'
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)
Vue.use(JsonViewer)

// Vue.use(VCharts)
Vue.prototype.$axios = axios

Vue.component('noData', noData)
Vue.component('pagination', pagination)
Vue.component('uploadImg', uploadImg)
Vue.component('imgManager', imgManager)

Vue.config.productionTip = false

new Vue({
  el: '#vue-admin-beautiful',
  router,
  store,
  render: h => h(App),
})
