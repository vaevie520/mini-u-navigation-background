import request from '@/utils/request'

// 站点分类列表
export function getCateList(params = {}) {
  return request({
    url: '/SiteCate/cateList',
    method: 'get',
    params,
  })
}


// 获取简洁分类
export function getSimpleCateList(params = {}) {
  return request({
    url: '/SiteCate/simpleList',
    method: 'get',
    params,
  })
}


// 添加站点分类
export function addCate(data) {
  return request({
    url: '/SiteCate/add',
    method: 'post',
    data,
  })
}

// 站点分类详情
export function getCateDetail(id) {
  return request({
    url: '/SiteCate/detail',
    method: 'get',
    params: {
      id
    },
  })
}

// 编辑站点分类
export function editCate(data) {
  return request({
    url: '/SiteCate/edit',
    method: 'post',
    data,
  })
}

// 编辑站点分类状态
export function setCateStatus(data) {
  return request({
    url: '/SiteCate/setStatus',
    method: 'post',
    data,
  })
}

