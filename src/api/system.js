import request from '@/utils/request'

// 获取访问日志
export function getVisitList(params = {}) {
  return request({
    url: '/System/visitLog',
    method: 'get',
    params,
  })
}

export function getErrList(params = {}) {
  return request({
    url: '/System/errorList',
    method: 'get',
    params,
  })
}
