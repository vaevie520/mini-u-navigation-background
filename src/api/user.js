import request from '@/utils/request'
import { encryptedData } from '@/utils/encrypt'
import { loginRSA } from '@/config/settings'

export async function login(data) {
  if (loginRSA) {
    data = await encryptedData(data)
  }
  return request({
    url: '/Admin/login',
    method: 'post',
    data,
  })
}

export function getInfo(token = '') {
  return request({
    url: '/Admin/userInfo',
    method: 'post',
    data: {
      token: '2d051900bcfb5aa760f74bca18f8fd57'
    }
  })
}

export function logout() {
  return request({
    url: '/Admin/logOut',
    method: 'get',
  })
}
export function register() {
  return request({
    url: '/register',
    method: 'post',
  })
}
