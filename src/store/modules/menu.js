/**
 * @copyright
 * @description 左侧菜单逻辑
 */

const state = {
  activeRoute: {},
}
const getters = {
  activeRoute: state => state.activeRoute,
}
const mutations = {
  // 页面刷新设置激活路由
  setActiveRoute(state, route) {
    state.activeRoute = route
  },
  showSubRoute(state, route) {},
  navigateTo(state, { vue, route }) {},
  changeActive(state, route) {
    route.active = true
    state.activeRoute = route
  },
}
const actions = {
  setRoutes(store, config = {}) {
    const { state, rootState } = store
    console.log(rootState)
  },
}
export default { state, getters, mutations, actions }
